import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  List<List> _gridValues;

  _MainPageState() {
    _initMatrix();
  }

  _initMatrix() {
    _gridValues = List<List>(3);
    for (var row = 0; row < _gridValues.length; row++) {
      _gridValues[row] = List(3);
      for (var column = 0; column < _gridValues[row].length; column++) {
        _gridValues[row][column] = ' ';
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Center(child: Text('Tic Tac Toe')),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  _grid(0, 0),
                  _grid(0, 1),
                  _grid(0, 2),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  _grid(1, 0),
                  _grid(1, 1),
                  _grid(1, 2),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  _grid(2, 0),
                  _grid(2, 1),
                  _grid(2, 2),
                ],
              ),
            ],
          ),
        ));
  }

  String _lastChar = 'o';

  _grid(int i, int j) {
    return GestureDetector(
      onTap: () {
        _changeGridValues(i, j);

        if (_checkWinner(i, j)) {
          _showDialog(_gridValues[i][j]);
        } else {
          if (_checkDraw()) {
            _showDialog(null);
          }
        }
      },
      child: Container(
        width: 100.0,
        height: 100.0,
        decoration: BoxDecoration(border: Border.all(color: Colors.blue)),
        child: Center(
          child: Text(
            _gridValues[i][j],
            style: TextStyle(fontSize: 90.0),
          ),
        ),
      ),
    );
  }

  _changeGridValues(int i, int j) {
    setState(() {
      if (_gridValues[i][j] == ' ') {
        if (_lastChar == 'O')
          _gridValues[i][j] = 'X';
        else
          _gridValues[i][j] = 'O';

        _lastChar = _gridValues[i][j];
      }
    });
  }

  _checkDraw() {
    var draw = true;
    _gridValues.forEach((i) {
      i.forEach((j) {
        if (j == ' ') draw = false;
      });
    });
    return draw;
  }

  _checkWinner(int x, int y) {
    var col = 0, row = 0, diag = 0, rdiag = 0;
    var n = _gridValues.length - 1;
    var player = _gridValues[x][y];

    for (int i = 0; i < _gridValues.length; i++) {
      if (_gridValues[x][i] == player) col++;
      if (_gridValues[i][y] == player) row++;
      if (_gridValues[i][i] == player) diag++;
      if (_gridValues[i][n - i] == player) rdiag++;
    }
    if (row == n + 1 || col == n + 1 || diag == n + 1 || rdiag == n + 1) {
      return true;
    }
    return false;
  }

  _showDialog(String winner) {
    String dialogText;
    if (winner == null) {
      dialogText = 'It\'s a draw';
    } else {
      dialogText = 'Player $winner won';
    }

    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Game over!'),
            content: Text(dialogText),
            actions: <Widget>[
              FlatButton(
                child: Text('Reset Game'),
                onPressed: () {
                  Navigator.of(context).pop();
                  setState(() {
                    _initMatrix();
                  });
                },
              ),
              FlatButton(
                child: Text('Exit'),
                onPressed: () {
                  setState(() {
                    Navigator.pop(context);
                  });
                },
              ),
            ],
          );
        });
  }
}
